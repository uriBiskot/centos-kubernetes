Virtual Machine For Kubernetes testing

Require:
- Vagrant 2.0+
- Plugins “vagrant-vbguest”
- VirtualBox 6.0+

### Informatión general
- For init all machines: **vagrant up**
- For external ssh: **private\_key**
**\~/CentosServer/vagrant/.vagrant/machines/web/virtualbox/private\_key**

# VM1 - Docker Machine

It’s you need init only this machine type `vagrant up web` 

This machine contain:
- NTP config for Madrid (you can changed)
- GIT
- Kubernetes
